<?php

/**
 * @file
 * Install, update and uninstall functions for the Publish to Apple News module.
 */

include_once 'includes/install_helpers.inc';

/**
 * Implements hook_enable().
 */
function publish_to_apple_news_enable() {
  // Ensure module executes after token
  db_update('system')
    ->fields(array(
      'weight' => 1,
    ))
    ->condition('type', 'module')
    ->condition('name', 'publish_to_apple_news')
    ->execute();
}

/**
 * Implements hook_schema().
 */
function publish_to_apple_news_schema() {
  $schema['publish_to_apple_news_article_types'] = array(
    'description' => 'The base table for Publish to Apple News article types.',
    'fields' => array(
      'tid' => array(
        'description' => 'The primary identifier of this article type.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'content_type' => array(
        'description' => 'The content type this article type will generate Apple News articles from.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'The Drupal-only description of this article type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'The article title.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'subtitle' => array(
        'description' => 'The article subtitle.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'excerpt' => array(
        'description' => 'A short summary of the contents of the article.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'language' => array(
        'description' => 'The language code of the article.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'metadata_thumbnail_url' => array(
        'description' => 'The URL of an image that can represent this document in a list of other documents. Note: only JPEG and PNG image types are supported.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'metadata_canonical_url' => array(
        'description' => 'The canonical URL of a web version of this document.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'metadata_video_url' => array(
        'description' => 'Defines the URL for the video that represents this article.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'metadata_authors' => array(
        'description' => "The names of the article's authors.",
        'type' => 'text',
        'size' => 'small',
        'not null' => TRUE,
      ),
      'metadata_keywords' => array(
        'description' => 'Keywords that describe the article. You can set up to 50 keywords.',
        'type' => 'text',
        'size' => 'small',
        'not null' => TRUE,
      ),
      'metadata_cover_art' => array(
        'description' => 'Cover art allows you to provide additional cover artwork for an article for editorial consideration.',
        'type' => 'text',
        'size' => 'medium',
      ),
      'layout_columns' => array(
        'description' => 'The number of columns the article was designed for.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'layout_width' => array(
        'description' => 'The width (in points) this article was designed for.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'layout_margin' => array(
        'description' => 'The outer (left and right) margins (in points) for this article.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'layout_gutter' => array(
        'description' => 'The gutter size (in points) to use for spacing between columns for this article.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'ad_frequency' => array(
        'description' => 'A number between 0 and 10 defining the frequency for automatically inserting ads.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'ad_margin_top' => array(
        'description' => 'Ad margin top',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'ad_margin_bottom' => array(
        'description' => 'Ad margin bottom',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('tid'),
  );
  $schema['publish_to_apple_news_component_layouts'] = array(
    'description' => 'The base table for Publish to Apple News component layouts.',
    'fields' => array(
      'lid' => array(
        'description' => 'The primary identifier of this component layout.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'tid' => array(
        'description' => 'The tid of the Publish to Apple News Article Type this layout belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'identifier' => array(
        'description' => 'The unique identifier for this layout to be referenced by components.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'column_start' => array(
        'description' => "Indicates which column the component's start position is in, based on the number of columns in the document.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'column_span' => array(
        'description' => 'Indicates how many columns the component spans, based on the number of columns in the document.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'margin_top' => array(
        'description' => 'The top margin in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'margin_bottom' => array(
        'description' => 'The bottom margin in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'content_inset_top' => array(
        'description' => 'Boolean that describes whether the top inset should be taken into account.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'content_inset_right' => array(
        'description' => 'Boolean that describes whether the right inset should be taken into account.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'content_inset_bottom' => array(
        'description' => 'Boolean that describes whether the bottom inset should be taken into account.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'content_inset_left' => array(
        'description' => 'Boolean that describes whether the left inset should be taken into account.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'ignore_document_margin' => array(
        'description' => "Indicates whether a document's margins should be respected or should be ignored by the component.",
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'ignore_document_gutter' => array(
        'description' => "Indicates whether the gutters (if any) to the left and right of the component should be ignored.",
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'minimum_height' => array(
        'description' => 'Describes the minimum height of the component.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('lid'),
    'foreign keys' => array(
      'layout_article_type' => array(
        'table' => 'publish_to_apple_news_article_types',
        'columns' => array('tid' => 'tid'),
      ),
    ),
  );
  $component_styles_schema = array(
    'description' => 'The base table for Publish to Apple News component styles.',
    'fields' => array(
      'sid' => array(
        'description' => 'The primary identifier of this component style.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'tid' => array(
        'description' => 'The tid of the Publish to Apple News Article Type this style belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'identifier' => array(
        'description' => 'The unique identifier for this style to be referenced by components.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'background_color' => array(
        'description' => "The component's background color, defined as a 3- to 8-character RGBA hexidecimal string.",
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'opacity' => array(
        'description' => 'The opacity of the component, set as a float value between 0 (completely transparent) and 1 (completely opaque).',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'border_color' => array(
        'description' => 'The stroke color, defined as a 3- to 8-character RGBA hexadecimal string.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'border_width' => array(
        'description' => 'The width of the stroke line. Can be either an integer value in points, or a string compliant with the supported units.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'border_style' => array(
        'description' => 'Defines the style of the stroke.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'border_top' => array(
        'description' => 'Indicates whether the border should be applied to the top.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'border_bottom' => array(
        'description' => 'Indicates whether the border should be applied to the bottom.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'border_left' => array(
        'description' => 'Indicates whether the border should be applied to the left.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'border_right' => array(
        'description' => 'Indicates whether the border should be applied to the right.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('sid'),
    'foreign keys' => array(
      'style_article_type' => array(
        'table' => 'publish_to_apple_news_article_types',
        'columns' => array('tid' => 'tid'),
      ),
    ),
  );

  $image_fill_schema = _publish_to_apple_news_component_fill_schema('image');
  $video_fill_schema = _publish_to_apple_news_component_fill_schema('video');
  $component_styles_schema['fields'] = array_merge($component_styles_schema['fields'], $image_fill_schema, $video_fill_schema);
  $schema['publish_to_apple_news_component_styles'] = $component_styles_schema;

  $component_text_style_schema = array(
    'description' => 'The base table for Publish to Apple News component text styles.',
    'fields' => array(
      'tsid' => array(
        'description' => 'The primary identifier of this component text style.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'tid' => array(
        'description' => 'The tid of the Publish to Apple News Article Type this text style belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'identifier' => array(
        'description' => 'The unique identifier for this text style to be referenced by components.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'font_name' => array(
        'description' => 'The PostScript name of the font to apply.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'font_size' => array(
        'description' => 'The size of the font, in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'text_color' => array(
        'description' => "The text color, defined as a 3- to 8-character RGBA hexadecimal string. Should include '#'.",
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'text_transform' => array(
        'description' => 'The transform to apply to the text.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'underline' => array(
        'description' => 'Whether to underline the text.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'strikethrough' => array(
        'description' => 'Whether to have a strike through the text.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'background_color' => array(
        'description' => "The background color for text lines, defined as a 3- to 8-character RGBA hexadecimal string. Should include '#'.",
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'vertical_alignment' => array(
        'description' => 'The vertical alignment of the text, allowing for superscript and subscript.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'tracking' => array(
        'description' => 'The amount of tracking for characters in text, in percentage of the font size, as a decimal.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'text_alignment' => array(
        'description' => 'Justification (horizontal alignment) style for all text within the component.',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'line_height' => array(
        'description' => 'The default line height, in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'paragraph_spacing_before' => array(
        'description' => 'Defines the spacing before each paragraph in points. Relative to the line height.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'paragraph_spacing_after' => array(
        'description' => 'Defines the spacing after each paragraph in points. Relative to the line height.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'first_line_indent' => array(
        'description' => 'Defines the indent of the first line of each paragraph in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'hyphenation' => array(
        'description' => 'Indicates whether the text should hyphenate when necessary.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'hanging_punctuation' => array(
        'description' => 'Defines whether punctuation should be positioned outside the margins of the body text.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'link_style_font_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'link_style_font_size' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'link_style_text_color' => array(
        'description' => "Should include '#'.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'link_style_underline' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_number_of_lines' => array(
        'description' => 'Indicates the number of text lines this drop cap should span. Minimum is 2, maximum is 6.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_number_of_characters' => array(
        'description' => 'Indicates the number of characters this drop cap should use. Minimum is 1, maximum is 4.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_number_of_raised_lines' => array(
        'description' => 'Describes the number of text lines this drop cap should raise.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_font_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'dropcap_text_color' => array(
        'description' => "Should include '#'.",
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_background_color' => array(
        'description' => "Should include '#'.",
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => 0,
      ),
      'dropcap_padding' => array(
        'description' => 'Sets the padding of the drop cap in points.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('tsid'),
    'foreign keys' => array(
      'text_style_article_type' => array(
        'table' => 'publish_to_apple_news_article_types',
        'columns' => array('tid' => 'tid'),
      ),
    ),
  );

  $text_shadow_field_schemas = _publish_to_apple_news_text_shadow_field_schemas();
  $component_text_style_schema['fields'] = array_merge($component_text_style_schema['fields'], $text_shadow_field_schemas);
  $schema['publish_to_apple_news_component_text_styles'] = $component_text_style_schema;

  $schema['publish_to_apple_news_posted_articles'] = array(
    'description' => 'The base table for posted Publish to Apple News article ids.',
    'fields' => array(
      'entity_type' => array(
        'description' => 'The entity type this data is attached to.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_id' => array(
        'description' => 'The entity id this data is attached to.',
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'article_id' => array(
        'description' => 'The ID of the posted article.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'share_url' => array(
        'description' => 'The article share URL. Can be opened by any device with Apple News',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'sections' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'revision' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'preview' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'accessory_text' => array(
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'maturity_rating' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
      ),
      'state' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'date_published' => array(
        'type' => 'varchar',
        'mysql_type' => 'datetime',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('entity_type', 'entity_id', 'article_id'),
    'unique keys' => array(
      'entity' => array('entity_type', 'entity_id'),
    ),
  );

  // Define schemas for each component type
  drupal_load('module', 'publish_to_apple_news');
  module_load_include('inc', 'publish_to_apple_news', 'includes/components/component_base');

  $component_types = publish_to_apple_news_component_types();
  foreach ($component_types as $entity_machine_name => $entity_class_name) {
    $include_file = str_replace('publish_to_apple_news_', 'includes/components/', $entity_machine_name);
    module_load_include('inc', 'publish_to_apple_news', $include_file);
    $class = new $entity_class_name(NULL);
    $schema[$entity_machine_name . 's'] = $class->schema();
  }

  return $schema;
}


/**
 * Add a state column to publish_to_apple_news_posted_articles table.
 */
function publish_to_apple_news_update_7102() {
  if (!db_field_exists('publish_to_apple_news_posted_articles', 'state')) {
    $spec = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );
    db_add_field('publish_to_apple_news_posted_articles', 'state', $spec);
  }
}

/**
 * Change 'not null' to TRUE for publish_to_apple_news_posted_articles.article_id
 */
function publish_to_apple_news_update_7103() {
  db_change_field('publish_to_apple_news_posted_articles', 'article_id', 'article_id', array(
    'description' => 'The ID of the posted article.',
    'type' => 'varchar',
    'length' => 128,
    'not null' => TRUE,
    'default' => '',
  ));
}

/**
 * Add a date_published column to publish_to_apple_news_posted_articles table.
 */
function publish_to_apple_news_update_7104() {
  if (!db_field_exists('publish_to_apple_news_posted_articles', 'date_published')) {
    $spec = array(
      'type' => 'varchar',
      'mysql_type' => 'datetime',
      'not null' => FALSE,
    );
    db_add_field('publish_to_apple_news_posted_articles', 'date_published', $spec);
  }
}

/**
 * Add advertisement settings to publish_to_apple_news_article_types table.
 */
function publish_to_apple_news_update_7105() {
  $ad_frequency = array(
    'description' => 'A number between 0 and 10 defining the frequency for automatically inserting ads.',
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
  );
  $ad_margin_top = array(
    'description' => 'Ad margin top',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  );
  $ad_margin_bottom = array(
    'description' => 'Ad margin bottom',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  );

  if (!db_field_exists('publish_to_apple_news_article_types', 'ad_frequency')) {
    db_add_field('publish_to_apple_news_article_types', 'ad_frequency', $ad_frequency);
  }
  if (!db_field_exists('publish_to_apple_news_article_types', 'ad_margin_top')) {
    db_add_field('publish_to_apple_news_article_types', 'ad_margin_top', $ad_margin_top);
  }
  if (!db_field_exists('publish_to_apple_news_article_types', 'ad_margin_bottom')) {
    db_add_field('publish_to_apple_news_article_types', 'ad_margin_bottom', $ad_margin_bottom);
  }
}

/**
 * Add schema for Apple News embed web video component.
 */
function publish_to_apple_news_update_7106() {
  $entity_machine_name = 'publish_to_apple_news_embed_web_video_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add schema for Apple News Tweet component.
 */
function publish_to_apple_news_update_7107() {
  $entity_machine_name = 'publish_to_apple_news_tweet_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add schema for Apple News Instagram component.
 */
function publish_to_apple_news_update_7108() {
  $entity_machine_name = 'publish_to_apple_news_instagram_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add schema for Apple News Facebook component.
 */
function publish_to_apple_news_update_7109() {
  $entity_machine_name = 'publish_to_apple_news_facebook_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add schema for Apple News Vine component.
 */
function publish_to_apple_news_update_7110() {
  $entity_machine_name = 'publish_to_apple_news_vine_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add Video URL column to article type settings table.
 */
function publish_to_apple_news_update_7111() {
  if (!db_field_exists('publish_to_apple_news_article_types', 'metadata_video_url')) {
    db_add_field('publish_to_apple_news_article_types', 'metadata_video_url', array(
      'description' => 'Defines the URL for the video that represents this article.',
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ));
  }
}

/**
 * Add schema for Apple News Video component.
 */
function publish_to_apple_news_update_7112() {
  $entity_machine_name = 'publish_to_apple_news_video_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add maturity_rating field to Apple News posted articles table.
 */
function publish_to_apple_news_update_7113() {
  if (!db_field_exists('publish_to_apple_news_posted_articles', 'maturity_rating')) {
    db_add_field('publish_to_apple_news_posted_articles', 'maturity_rating', array(
      'type' => 'varchar',
      'length' => 50,
      'not null' => TRUE,
      'default' => '',
    ));
  }
}

/**
 * Add metadata_cover_art field to Apple News posted articles table.
 */
function publish_to_apple_news_update_7115() {
  if (!db_field_exists('publish_to_apple_news_article_types', 'metadata_cover_art')) {
    db_add_field('publish_to_apple_news_article_types', 'metadata_cover_art', array(
      'description' => 'Cover art allows you to provide additional cover artwork for an article for editorial consideration.',
      'type' => 'text',
      'size' => 'medium',
    ));
  }
}

/**
 * Add paragraph spacing fields to Apple News component text styles table.
 */
function publish_to_apple_news_update_7116() {
  if (!db_field_exists('publish_to_apple_news_component_text_styles', 'paragraph_spacing_before')) {
    db_add_field('publish_to_apple_news_component_text_styles', 'paragraph_spacing_before', array(
      'description' => 'Defines the spacing before each paragraph in points. Relative to the line height.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
  if (!db_field_exists('publish_to_apple_news_component_text_styles', 'paragraph_spacing_after')) {
    db_add_field('publish_to_apple_news_component_text_styles', 'paragraph_spacing_after', array(
      'description' => 'Defines the spacing after each paragraph in points. Relative to the line height.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
}

/**
 * Add hanging punctuation field to Apple News component text styles table.
 */
function publish_to_apple_news_update_7117() {
  if (!db_field_exists('publish_to_apple_news_component_text_styles', 'hanging_punctuation')) {
    db_add_field('publish_to_apple_news_component_text_styles', 'hanging_punctuation', array(
      'description' => 'Defines whether punctuation should be positioned outside the margins of the body text.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
}

/**
 * Add first line indent field to Apple News component text styles table.
 */
function publish_to_apple_news_update_7118() {
  if (!db_field_exists('publish_to_apple_news_component_text_styles', 'first_line_indent')) {
    db_add_field('publish_to_apple_news_component_text_styles', 'first_line_indent', array(
      'description' => 'Defines the indent of the first line of each paragraph in points.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
}

/**
 * Add number of raised lines field to Apple News component text styles table.
 */
function publish_to_apple_news_update_7119() {
  if (!db_field_exists('publish_to_apple_news_component_text_styles', 'dropcap_number_of_raised_lines')) {
    db_add_field('publish_to_apple_news_component_text_styles', 'dropcap_number_of_raised_lines', array(
      'description' => 'Describes the number of text lines this drop cap should raise.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ));
  }
}

/**
 * Add text shadow fields to Apple News component text styles table.
 */
function publish_to_apple_news_update_7120() {
  $text_styles_table = 'publish_to_apple_news_component_text_styles';
  $text_shadow_field_schemas = _publish_to_apple_news_text_shadow_field_schemas();

  foreach ($text_shadow_field_schemas as $field_name => $field_schema) {
    if (!db_field_exists($text_styles_table, $field_name)) {
      db_add_field($text_styles_table, $field_name, $field_schema);
    }
  }
}

/**
 * Add accessory text field to Apple News posted articles table.
 */
function publish_to_apple_news_update_7121() {
  if (!db_field_exists('publish_to_apple_news_posted_articles', 'accessory_text')) {
    db_add_field('publish_to_apple_news_posted_articles', 'accessory_text', array(
      'type' => 'varchar',
      'length' => 100,
      'not null' => TRUE,
      'default' => '',
    ));
  }
}

/**
 * Add video fill fields to Apple News component styles table.
 */
function publish_to_apple_news_update_7122() {
  $component_styles_table = 'publish_to_apple_news_component_styles';
  $video_fill_schema = _publish_to_apple_news_component_fill_schema('video');

  foreach ($video_fill_schema as $field_name => $field_schema) {
    if (!db_field_exists($component_styles_table, $field_name)) {
      db_add_field($component_styles_table, $field_name, $field_schema);
    }
  }
}

/**
 * Add schema for Apple News map component.
 */
function publish_to_apple_news_update_7123() {
  $entity_machine_name = 'publish_to_apple_news_map_component';
  _publish_to_apple_news_install_component($entity_machine_name);
}

/**
 * Add new Publish to Apple News permissions to administrator role.
 */
function publish_to_apple_news_update_7124() {
  $permissions_definition = publish_to_apple_news_permission();
  $permissions = array();

  foreach ($permissions_definition as $permission => $definition) {
    $permissions[] = $permission;
  }

  // 3 is always Administrator role
  user_role_grant_permissions(3, $permissions);
}
