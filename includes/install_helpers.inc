<?php

/**
 * @file
 * Install and update helper functions
 */

/**
 * Helper function to install a new component (create db table)
 */
function _publish_to_apple_news_install_component($entity_machine_name) {
  $component_types = publish_to_apple_news_component_types();
  $entity_class_name = $component_types[$entity_machine_name];
  
  if (!db_table_exists($entity_machine_name . 's')) {
    drupal_load('module', 'publish_to_apple_news');
    module_load_include('inc', 'publish_to_apple_news', 'includes/components/component_base');
    $include_file = str_replace('publish_to_apple_news_', 'includes/components/', $entity_machine_name);
    module_load_include('inc', 'publish_to_apple_news', $include_file);
    $class = new $entity_class_name(NULL);
    db_create_table($entity_machine_name . 's', $class->schema());
  }
}

/**
 * Component text style text shadow fields
 */
function _publish_to_apple_news_text_shadow_field_schemas() {
  return array(
    'text_shadow_color' => array(
      'description' => 'The stroke color, defined as a 3- to 8-character RGBA hexadecimal string.',
      'type' => 'varchar',
      'length' => 30,
      'not null' => TRUE,
      'default' => '',
    ),
    'text_shadow_radius' => array(
      'description' => 'The shadow radius as a value between 0 and 100 in points.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
    'text_shadow_opacity' => array(
      'description' => 'Opacity of the shadow as a value between 0 and 1.',
      'type' => 'varchar',
      'length' => 10,
      'not null' => TRUE,
      'default' => '',
    ),
    'text_shadow_offset_x' => array(
      'description' => 'The shadow horizontal offset as a value between -50 and 50 in points.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
    'text_shadow_offset_y' => array(
      'description' => 'The shadow vertical offset as a value between -50 and 50 in points.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
  );
}

/**
 * Build the schema for a component style fill. Supports "image" and "video" fills.
 */
function _publish_to_apple_news_component_fill_schema($type = 'image') {
  $schema = array(
    $type . '_fill_attachment' => array(
      'description' => 'Indicates how the fill should behave when a user scrolls.',
      'type' => 'varchar',
      'length' => 30,
      'not null' => TRUE,
      'default' => '',
    ),
    $type . '_fill_url' => array(
      'description' => 'The URL of the image file to use for filling the component.',
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ),
    $type . '_fill_mode' => array(
      'description' => 'Indicates how the image fill should be displayed.',
      'type' => 'varchar',
      'length' => 30,
      'not null' => TRUE,
      'default' => '',
    ),
    $type . '_fill_vertical_alignment' => array(
      'description' => 'The vertical alignment of the image fill within its component.',
      'type' => 'varchar',
      'length' => 30,
      'not null' => TRUE,
      'default' => '',
    ),
    $type . '_fill_horizontal_alignment' => array(
      'description' => 'The horizontal alignment of the image fill within its component.',
      'type' => 'varchar',
      'length' => 30,
      'not null' => TRUE,
      'default' => '',
    ),
  );
  
  if ($type == 'video') {
    $schema['video_fill_url']['description'] = 'The URL of a video file that can be played using AVPlayer (M3u8 recommended).';
    $schema['video_fill_still_url'] = array(
      'description' => 'The URL of the image file to use as a still image when the video is not playing.',
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' =>  '',
    );
    $schema['video_fill_loop'] = array(
      'description' => 'When present, it specifies that the video will start over again when it reaches the end.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );
  }
  
  return $schema;
}
