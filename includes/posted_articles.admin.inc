<?php

/**
 * @page
 * Administrative report of processing and completed Apple News articles
 */

/**
 * Admin list of posted Apple News Articles.
 * @param $state - 'processing' or 'completed' - Filter posted articles
 */
function publish_to_apple_news_posted_page($state) {
  $render = array();
  
  $states = array();
  if ($state == 'processing') {
    $states = array(
      'PROCESSING',
      'PROCESSING_UPDATE',
    );
  } else {
    $states = array(
      'LIVE',
      'TAKEN_DOWN',
      'FAILED_PROCESSING',
      'FAILED_PROCESSING_UPDATE',
    );
  }
  $posted_articles = db_select('publish_to_apple_news_posted_articles', 'a')
    ->fields('a')
    ->condition('state', $states, 'IN')
    ->execute();
  
  $rows = array();
  foreach ($posted_articles as $article) {
    // Build table rows
    
    // Add link to node
    $node = node_load($article->entity_id);
    $node_path = drupal_get_path_alias('node/' . $node->nid);
    $node_link = l($node->title, $node_path);
    
    // Add Apple News share url link
    $share_link = l($article->share_url, $article->share_url);
    
    // Add section title
    $article_sections = explode(',', $article->sections);
    $article_section_names = array();
    $available_sections = variable_get('publish_to_apple_news_available_sections', array());
    foreach ($article_sections as $section) {
      if (!empty($available_sections[$section])) {
        $article_section_names[] = $available_sections[$section];
      }
    }
    $article_section_names = implode(', ', $article_section_names);
    
    // Show yes/no for preview field
    $is_preview = $article->preview ? t('Yes') : t('No');
    $rows[] = array(
      $node_link,
      $article->state,
      $article->article_id,
      $share_link,
      $article_section_names,
      $is_preview,
    );
  }
  
  $render['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Node'),
      t('State'),
      t('Article ID'),
      t('Share URL'),
      t('Sections'),
      t('Preview?'),
    ),
    '#rows' => $rows,
  );
  
  return $render;
}


function publish_to_apple_news_check_states_page() {
  publish_to_apple_news_check_states();
  drupal_goto('/admin/reports/publish-to-apple-news');
}