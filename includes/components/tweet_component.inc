<?php

/**
 * @file
 * Publish to Apple News tweet component definition
 */

class AppleNewsTweetComponent extends AppleNewsComponent {

  public function __construct($article_type, $entity = NULL) {
    parent::__construct($article_type, $entity);

    $this->entityMachineName = 'publish_to_apple_news_tweet_component';
    $this->name = t('Tweet');
    $this->role = 'tweet';
    $this->supportsReplacementPatterns = TRUE;
  }

  public function schema() {
    $schema = parent::schema();

    $schema['fields']['url'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );

    return $schema;
  }

  public function generateJson($node, &$files = array(), &$failedStyles) {
    $json = parent::generateJson($node, $files, $failedStyles);
    $json['URL'] = publish_to_apple_news_process_text_value($this->entity->url, array('node' => $node));
    
    return $json;
  }

  public function form(&$form_state) {
    $form = parent::form($form_state);

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('The URL of the tweet you want to embed.') . ' ' . $this->tokensDescription(),
      '#required' => TRUE,
    );

    return $form;
  }
}
