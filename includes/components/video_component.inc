<?php

/**
 * @file
 * Publish to Apple News Video component definition
 */

class AppleNewsVideoComponent extends AppleNewsComponent {

  public function __construct($article_type, $entity = NULL) {
    parent::__construct($article_type, $entity);

    $this->entityMachineName = 'publish_to_apple_news_video_component';
    $this->name = t('Video');
    $this->role = 'video';
    $this->supportsReplacementPatterns = TRUE;
  }

  public function schema() {
    $schema = parent::schema();

    $schema['fields']['url'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['still_url'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['aspect_ratio'] = array(
      'type' => 'varchar',
      'length' => 10,
      'not null' => TRUE,
      'default' => '',
    );

    return $schema;
  }

  public function generateJson($node, &$files = array(), &$failedStyles) {
    $json = parent::generateJson($node, $files, $failedStyles);
    $videoUrl = publish_to_apple_news_process_text_value($this->entity->url, array('node' => $node));
    
    if (!_publish_to_apple_news_validate_video_url($videoUrl, 'video')) {
      drupal_set_message(t('The URL entered into Video component @identifier contains a Youtube or Vimeo URL. ' . 
        'It should contain a URL to a video file (.m3u8 recommended). Use the Embed Web Video component for Youtube/Vimeo.', array(
        '@identifier' => $this->entity->identifier,
      )), 'error');
      return FALSE;
    }
    
    $json['URL'] = $videoUrl;
    
    if (!empty($this->entity->still_url)) {
      $still_url = publish_to_apple_news_process_text_value($this->entity->still_url, array('node' => $node));
      $imageData = publish_to_apple_news_convert_image($still_url);
      
      if ($imageData) {
        $json['stillURL'] = $imageData['bundle'];
        
        if (!in_array($imageData['realpath'], $files)) {
          $files[] = $imageData['realpath'];
        }
      }
    }
    
    if (!empty($this->entity->aspect_ratio)) {
      $json['aspectRatio'] = (float) $this->entity->aspect_ratio;
    }
    
    return $json;
  }

  public function form(&$form_state) {
    $form = parent::form($form_state);

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('The URL of a video file that can be played using AVPlayer. HTTP Live Streaming is highly recommended (.M3U8)') . ' ' . $this->tokensDescription(),
      '#required' => TRUE,
    );
    $form['still_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Still URL'),
      '#description' => t('Image that should be shown when the video has not yet been played.') . ' ' . $this->tokensDescription(),
    );
    $form['aspect_ratio'] = array(
      '#type' => 'textfield',
      '#title' => t('Aspect ratio'),
      '#description' => t('Width divided by height. Leave empty for 1.777 (16:9)'),
    );

    return $form;
  }
}
