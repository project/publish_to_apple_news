<?php

/**
 * @file
 * Publish to Apple News Map component definition
 */

class AppleNewsMapComponent extends AppleNewsComponent {

  public function __construct($article_type, $entity = NULL) {
    parent::__construct($article_type, $entity);

    $this->entityMachineName = 'publish_to_apple_news_map_component';
    $this->name = t('Map');
    $this->role = 'map';
    $this->supportsReplacementPatterns = TRUE;
  }

  public function schema() {
    $schema = parent::schema();

    $schema['fields']['caption'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['latitude'] = array(
      'type' => 'varchar',
      'length' => 40,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['longitude'] = array(
      'type' => 'varchar',
      'length' => 40,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['items'] = array(
      'type' => 'text',
      'size' => 'medium'
    );
    $schema['fields']['map_type'] = array(
      'type' => 'varchar',
      'length' => 20,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['span_latitude_delta'] = array(
      'type' => 'varchar',
      'length' => 10,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['span_longitude_delta'] = array(
      'type' => 'varchar',
      'length' => 10,
      'not null' => TRUE,
      'default' => '',
    );

    return $schema;
  }

  public function generateJson($node, &$files = array(), &$failedStyles) {
    $json = parent::generateJson($node, $files, $failedStyles);
    $json['caption'] = publish_to_apple_news_process_text_value($this->entity->caption, array('node' => $node));
    $json['mapType'] = $this->entity->map_type;

    if (!empty($this->entity->latitude)) {
      $json['latitude'] = (float)publish_to_apple_news_process_text_value($this->entity->latitude, array('node' => $node));
    }
    if (!empty($this->entity->longitude)) {
      $json['longitude'] = (float)publish_to_apple_news_process_text_value($this->entity->longitude, array('node' => $node));
    }

    $items = json_decode($this->entity->items, TRUE);
    if (!empty($items[0]['latitude'])) {
      $json['items'] = $items;
    }

    if (!empty($this->entity->span_latitude_delta)) {
      $json['span'] = array(
        'latitudeDelta' => (float)$this->entity->span_latitude_delta,
        'longitudeDelta' => (float)$this->entity->span_longitude_delta,
      );
    }

    return $json;
  }

  public function form(&$form_state) {
    $form = parent::form($form_state);
    $form['#tree'] = TRUE;

    $form['map_type'] = array(
      '#type' => 'select',
      '#options' => array(
        'standard' => t('Standard'),
        'hybrid' => t('Hybrid'),
        'satellite' => t('Satellite'),
      ),
      '#title' => t('Map type'),
      '#description' => t('Use this to define the type of map to display by default.'),
    );
    $form['caption'] = array(
      '#type' => 'textfield',
      '#title' => t('Caption'),
      '#description' => t('A string that describes what is displayed on the map.'),
      '#required' => TRUE,
    );
    $form['latitude'] = array(
      '#type' => 'textfield',
      '#title' => 'Latitude',
      '#description' => t("The latitude of the map's center. Provide both a latitude and longitude, or an array of items.") . ' ' . $this->tokensDescription(),
    );
    $form['longitude'] = array(
      '#type' => 'textfield',
      '#title' => 'Longitude',
      '#description' => t("The longitude of the map's center. Provide both a latitude and longitude, or an array of items.") . ' ' . $this->tokensDescription(),
    );

    $items = json_decode($this->entity->items, TRUE);
    if (empty($form_state['num_items'])) {
      $form_state['num_items'] = count($items);
    }
    if ($form_state['num_items'] < 1) {
      $form_state['num_items'] = 1;
    }
    $form['items'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Items'),
      '#description' => t('Map items. If latitude and longitude are not set, at least one map item should exist.'),
      '#prefix' => '<div id="map-items">',
      '#suffix' => '</div>',
    );
    for ($i = 0; $i < $form_state['num_items']; $i++) {
      $form['items']['item'][$i] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#title' => t('Item ' . ($i + 1)),
        'item_latitude' => array(
          '#type' => 'textfield',
          '#title' => t('Latitude'),
          '#default_value' => !empty($items[$i]) ? $items[$i]['latitude'] : '',
        ),
        'item_longitude' => array(
          '#type' => 'textfield',
          '#title' => t('Longitude'),
          '#default_value' => !empty($items[$i]) ? $items[$i]['longitude'] : '',
        ),
        'item_caption' => array(
          '#type' => 'textfield',
          '#title' => t('Caption'),
          '#default_value' => !empty($items[$i]) ? $items[$i]['caption'] : '',
        ),
      );
    }
    $form['items']['add_item'] = array(
      '#type' => 'submit',
      '#value' => t('Add item'),
      '#submit' => array('publish_to_apple_news_add_map_item'),
      '#ajax' => array(
        'callback' => 'publish_to_apple_news_map_item_callback',
        'wrapper' => 'map-items',
      ),
    );
    $form['items']['remove_item'] = array(
      '#type' => 'submit',
      '#value' => t('Remove item'),
      '#submit' => array('publish_to_apple_news_remove_map_item'),
      '#ajax' => array(
        'callback' => 'publish_to_apple_news_map_item_callback',
        'wrapper' => 'map-items',
      ),
    );

    $form['span'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Map Span'),
      '#description' => t('Defines the visible bounds for a map using deltas from a defined center coordinate.'),
      'span_latitude' => array(
        '#type' => 'textfield',
        '#title' => t('Latitude delta'),
        '#description' => t('A float value between 0.0 and 90.0.'),
        '#default_value' => !empty($this->entity->span_latitude_delta) ? $this->entity->span_latitude_delta : '',
      ),
      'span_longitude' => array(
        '#type' => 'textfield',
        '#title' => t('Longitude delta'),
        '#description' => t('A float value between 0.0 and 180.0.'),
        '#default_value' => !empty($this->entity->span_longitude_delta) ? $this->entity->span_longitude_delta : '',
      ),
    );

    return $form;
  }

  public function saveExtraProperties(&$component, &$form_state) {
    if (!empty($form_state['values']['items'])) {
      $items = array();
      foreach ($form_state['values']['items']['item'] as $item) {
        $items[] = array(
          'latitude' => (float)$item['item_latitude'],
          'longitude' => (float)$item['item_longitude'],
          'caption' => $item['item_caption'],
        );
      }

      if (!empty($items)) {
        $component->items = json_encode($items);
      }
    }

    if (!empty($form_state['values']['span']['span_latitude'])) {
      $component->span_latitude_delta = $form_state['values']['span']['span_latitude'];
      $component->span_longitude_delta = $form_state['values']['span']['span_longitude'];
    }
  }
}