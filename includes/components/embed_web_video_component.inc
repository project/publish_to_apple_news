<?php

/**
 * @file
 * Publish to Apple News embed web video component definition
 */

class AppleNewsEmbedWebVideoComponent extends AppleNewsComponent {

  public function __construct($article_type, $entity = NULL) {
    parent::__construct($article_type, $entity);

    $this->entityMachineName = 'publish_to_apple_news_embed_web_video_component';
    $this->name = t('Embed Web Video');
    $this->role = 'embedwebvideo';
    $this->supportsReplacementPatterns = TRUE;
  }

  public function schema() {
    $schema = parent::schema();

    $schema['fields']['url'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );
    $schema['fields']['caption'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    );

    return $schema;
  }

  public function generateJson($node, &$files = array(), &$failedStyles) {
    $json = parent::generateJson($node, $files, $failedStyles);
    $url = publish_to_apple_news_process_text_value($this->entity->url, array('node' => $node));
    
    if (!_publish_to_apple_news_validate_video_url($url, 'embed')) {
      drupal_set_message(t('The URL entered into Embed Web Video component @identifier does not contain a Youtube or Vimeo URL. ' . 
        'Use the Video component for other types of videos.', array(
        '@identifier' => $this->entity->identifier,
      )), 'error');
      return FALSE;
    }
    
    $json['URL'] = $url;

    $caption = publish_to_apple_news_process_text_value($this->entity->caption, array('node' => $node));
    if (!empty($caption) && !$this->anyTokensLeft($caption)) {
      $json['caption'] = $caption;
    }
    
    return $json;
  }

  public function form(&$form_state) {
    $form = parent::form($form_state);

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a YouTube or Video embed URL.') . ' ' . $this->tokensDescription(),
      '#required' => TRUE,
    );
    $form['caption'] = array(
      '#type' => 'textfield',
      '#title' => t('Caption'),
      '#description' => $this->tokensDescription(),
    );

    return $form;
  }
}
