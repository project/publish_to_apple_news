<?php

/**
 * @file
 * Functions for generating a Publish to Apple News article.
 */

/**
 * Generate the json for an Apple News article.
 *
 * @param $node
 *  The fully-loaded node to generate the article from.
 *
 * @return array
 *  An array representing the entire article, ready to be json_encode'd, or FALSE on failure.
 */
function publish_to_apple_news_generate_article($node) {
  // Get the article type mapped to this node type
  $typeQuery = new EntityFieldQuery();
  $typeResult = $typeQuery->entityCondition('entity_type', 'publish_to_apple_news_article_type')
    ->propertyCondition('content_type', $node->type)
    ->range(0, 1)
    ->execute();

  if (!isset($typeResult['publish_to_apple_news_article_type'])) {
    drupal_set_message(t('No article type found for the @node_type node type.', array(
      '@node_type' => $node->type,
    )), 'error');

    return FALSE;
  } else {
    $ids = array_keys($typeResult['publish_to_apple_news_article_type']);
    $article_type = entity_load_single('publish_to_apple_news_article_type', $ids[0]);
    $files = array();

    $json = publish_to_apple_news_generate_article_top_level($article_type, $node, $files);

    $componentLayouts = publish_to_apple_news_generate_component_layouts($article_type);
    $failedStyles = array();
    $componentStyles = publish_to_apple_news_generate_component_styles($article_type, $node, $files, $failedStyles);
    $componentTextStyles = publish_to_apple_news_generate_component_text_styles($article_type);
    $components = publish_to_apple_news_generate_components($article_type, $node, $files, 0, '', $failedStyles);

    if (!$components) {
      return FALSE;
    }

    if (!empty($json['metadata']['videoURL'])) {
      $isVideoValid = publish_to_apple_news_validate_video_url($json['metadata']['videoURL'], $components);
      if (! $isVideoValid) {
        drupal_set_message(t('Apple News - The video URL defined on the article type does not match any video components. ' .
        'Remove the video URL defined in !settings_link or add a video component with the same URL.', array(
          '!settings_link' => l('settings', '/admin/structure/publish-to-apple-news/types/' . $article_type->tid)
        )), 'error');
        return FALSE;
      } else {
        // Set metadata thumbnailURL (overriding if not empty) to video's still URL
        // Locate video component
        $video_component_entity_name = 'publish_to_apple_news_video_component';
        $query = new EntityFieldQuery();
        $result = $query->entityCondition('entity_type', $video_component_entity_name)
          ->propertyCondition('tid', $article_type->tid)
          ->propertyCondition('url', $json['metadata']['videoURL'])
          ->execute();

        if (!empty($result[$video_component_entity_name])) {
          $ids = array_keys($result[$video_component_entity_name]);
          $video_component = entity_load_single($video_component_entity_name, $ids[0]);
          $thumbnailURL = publish_to_apple_news_process_text_value($video_component->still_url, array(
            'node' => $node,
          ));
          $imageData = publish_to_apple_news_convert_image($thumbnailURL);

          if ($imageData) {
            $json['metadata']['thumbnailURL'] = $imageData['bundle'];
          }
        }
      }
    }

    if (!empty($components)) {
      $json['components'] = $components;
    }
    if (!empty($componentLayouts)) {
      $json['componentLayouts'] = $componentLayouts;
    }
    if (!empty($componentStyles)) {
      $json['componentStyles'] = $componentStyles;
    }
    if (!empty($componentTextStyles)) {
      $json['componentTextStyles'] = $componentTextStyles;
    }

    return array(
      'article' => $json,
      'files' => $files,
    );
  }
}

/**
 * Validate the metadata video URL supplied matches the URL of one of the video components.
 *
 * @param $video_url - metadata token-processed videoURL
 * @param $components - article's components
 */
function publish_to_apple_news_validate_video_url($video_url, $components) {
  $all_video_urls = publish_to_apple_news_get_component_video_urls($components);

  if (!in_array($video_url, $all_video_urls)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Recursively get URLs from all video components in the article.
 */
function publish_to_apple_news_get_component_video_urls($components) {
  $all_video_urls = array();

  foreach ($components as $component) {
    if ($component['role'] == 'video') {
      $all_video_urls[] = $component['URL'];
    }
    if (!empty($component['components'])) {
      $child_video_urls = publish_to_apple_news_get_component_video_urls($component['components']);
      $all_video_urls += $child_video_urls;
    }
  }

  return $all_video_urls;
}

/**
 * Generates the json for the top level of the Apple News article.
 *
 * @param $article_type
 *  The fully-loaded article type.
 * @param $node
 *  The fully-loaded node.
 * @param $files
 *  An array of full file paths to append to.
 *
 * @return array
 *  An array ready to be json_encode'd
 */
function publish_to_apple_news_generate_article_top_level($article_type, $node, &$files) {
  global $base_url;

  // Default to english language
  $language = publish_to_apple_news_process_text_value($article_type->language, array('node' => $node));
  if ($language == 'und') {
    $language = 'en';
  }

  $json = array(
    'version' => APPLE_NEWS_FORMAT_VERSION,
    'identifier' => publish_to_apple_news_process_text_value(variable_get('publish_to_apple_news_article_identifier', '[site:name]--[node:nid]'), array('node' => $node)),
    'language' => $language,
    'title' => publish_to_apple_news_process_text_value($article_type->title, array('node' => $node)),
    'layout' => array(
      'columns' => (int)$article_type->layout_columns,
      'width' => (int)$article_type->layout_width,
      'margin' => (int)$article_type->layout_margin,
      'gutter' => (int)$article_type->layout_gutter,
    ),
  );

  if (!empty($article_type->subtitle)) {
    $json['subtitle'] = publish_to_apple_news_process_text_value($article_type->subtitle, array('node' => $node));
  }

  // Add advertising settings
  if ($article_type->ad_frequency) {
    $json['advertisingSettings'] = array(
      'frequency' => (int)$article_type->ad_frequency,
      'layout' => array(
        'margin' => array(
          'top' => $article_type->ad_margin_top,
          'bottom' => $article_type->ad_margin_bottom,
        ),
      ),
    );
  }

  // Add generator id, name, and version
  $json['metadata'] = array(
    'generatorIdentifier' => 'DrupalPublishToAppleNews',
    'generatorName' => 'Publish to Apple News Drupal Module',
    'generatorVersion' => APPLE_NEWS_MODULE_VERSION,
  );

  if (!empty($article_type->excerpt)) {
    $json['metadata']['excerpt'] = publish_to_apple_news_process_text_value($article_type->excerpt, array('node' => $node));
  }

  if (!empty($article_type->metadata_thumbnail_url)) {
    $thumbnailURL = publish_to_apple_news_process_text_value($article_type->metadata_thumbnail_url, array('node' => $node));
    $imageData = publish_to_apple_news_convert_image($thumbnailURL);

    if ($imageData) {
      $json['metadata']['thumbnailURL'] = $imageData['bundle'];
      $files[] = $imageData['realpath'];
    }
  }

  if (!empty($article_type->metadata_canonical_url)) {
    $canonical_url = publish_to_apple_news_process_text_value($article_type->metadata_canonical_url, array('node' => $node));
    $live_base_url = variable_get('publish_to_apple_news_live_base_url', '');
    if ($live_base_url) {
      // Replace w/ live base URL from settings
      $canonical_url = str_replace($base_url, $live_base_url, $canonical_url);
    }
    $json['metadata']['canonicalURL'] = $canonical_url;
  }

  if (!empty($article_type->metadata_video_url)) {
    $video_url = publish_to_apple_news_process_text_value($article_type->metadata_video_url, array('node' => $node));
    $json['metadata']['videoURL'] = $video_url;
  }

  if (!empty($article_type->metadata_authors)) {
    $metadata_authors = json_decode($article_type->metadata_authors);
    $authors = array();
    foreach ($metadata_authors as $author) {
      $author_processed = publish_to_apple_news_process_text_value($author, array('node' => $node));
      if (!empty($author_processed)) {
        $authors[] = $author_processed;
      }
    }
    $json['metadata']['authors'] = $authors;
  }

  if (!empty($article_type->metadata_keywords)) {
    $metadata_keywords = json_decode($article_type->metadata_keywords);
    $keywords = array();
    foreach ($metadata_keywords as $keyword) {
      $keyword_processed = publish_to_apple_news_process_text_value($keyword, array('node' => $node));
      if (!empty($keyword_processed)) {
        $keywords[] = $keyword_processed;
      }
    }
    $json['metadata']['keywords'] = $keywords;
  }

  // Add cover art
  if (!empty($article_type->metadata_cover_art)) {
    $metadata_cover_art = json_decode($article_type->metadata_cover_art);
    $cover_arts = array();
    foreach ($metadata_cover_art as $cover_art) {
      $imageUrl = publish_to_apple_news_process_text_value($cover_art, array('node' => $node));
      $imageData = publish_to_apple_news_convert_image($imageUrl);

      if ($imageData) {
        $cover_arts[] = array(
          'type' => 'image',
          'URL' => $imageData['bundle'],
        );
        $files[] = $imageData['realpath'];
      }
    }
    if (!empty($cover_arts)) {
      $json['metadata']['coverArt'] = $cover_arts;
    }
  }

  // Add dateCreated, dateModified, and datePublished
  $created_time = !empty($node->created) ? $node->created : time();
  $modified_time = time();

  // Get published date from db if exists
  $published_time = time();
  $result = db_select('publish_to_apple_news_posted_articles', 'p')
    ->fields('p', array('date_published', 'preview'))
    ->condition('entity_type', 'node')
    ->condition('entity_id', $node->nid)
    ->execute();
  $record = $result->fetchAssoc();
  if (!empty($record['date_published'])) {
    $published_time = strtotime($record['date_published']);
  }
  // Remove published date if article is still in preview
  if (!empty($record['preview'])) {
    $published_time = NULL;
  }

  // Add dates to metadata
  $json['metadata']['dateCreated'] = date(DATE_ATOM, $created_time);
  $json['metadata']['dateModified'] = date(DATE_ATOM, $modified_time);
  if ($published_time) {
    $json['metadata']['datePublished'] = DATE(DATE_ATOM, $published_time);
  }

  return $json;
}

/**
 * Generate the json for all of the components in a given article type.
 *
 * @param $article_type
 *  The fully-loaded article type to get the components from.
 * @param $node
 *  The fully-loaded node to replace tokens with.
 * @param $files
 *  An array of full file paths to append to.
 * @param $parent_cid
 *  The cid of the parent component to get all child components from.
 *  This function calls itself recursively.
 * @param $parent_type
 *  The entity machine name of the parent component's type.
 * @param $failedStyles
 *  An array of styles that were defined, but resolved to empty.
 *  Used to prevent referencing empty styles
 *
 * @return array
 *  An array of components ready to be json_encode'd.
 */
function publish_to_apple_news_generate_components($article_type, $node, &$files, $parent_cid = 0, $parent_type = '', &$failedStyles) {
  $json = array();
  $component_types = publish_to_apple_news_component_types();
  $component_entity_types = array_keys($component_types);

  $components = array();
  foreach ($component_entity_types as $component_entity_type) {
    $componentsQuery = new EntityFieldQuery();
    $componentsResult = $componentsQuery->entityCondition('entity_type', $component_entity_type)
      ->propertyCondition('parent_cid', $parent_cid)
      ->propertyCondition('parent_type', $parent_type)
      ->propertyCondition('tid', $article_type->tid)
      ->propertyOrderBy('weight')
      ->execute();

    if (!empty($componentsResult[$component_entity_type])) {
      $ids = array_keys($componentsResult[$component_entity_type]);
      foreach ($ids as $id) {
        $component = entity_load_single($component_entity_type, $id);
        $component->entity_type = $component_entity_type;
        $components[$component->weight] = $component;
      }
    }
  }

  if (!empty($components)) {
    ksort($components);
  }

  foreach ($components as $component) {
    $class = new $component_types[$component->entity_type]($article_type, $component);

    $componentJson = $class->generateJson($node, $files, $failedStyles);

    if (!$componentJson) {
      return FALSE;
    }

    if (!empty($componentJson)) {
      if ($class->acceptsComponents) {
        $childComponents = publish_to_apple_news_generate_components($article_type, $node, $files, $component->cid, $component->entity_type, $failedStyles);

        if (!empty($childComponents)) {
          $componentJson['components'] = $childComponents;
        }
      }

      if (array_values($componentJson) !== $componentJson) {
        // Associative array - just one component
        $json[] = $componentJson;
      } else {
        // Sequential array - The component has generated multiple components - see body component
        foreach ($componentJson as $generatedComponent) {
          $json[] = $generatedComponent;
        }
      }
    }
  }

  return $json;
}

/**
 * Generate the json for the article's componentLayouts.
 *
 * @param $article_type
 *  The fully-loaded article type to get the layouts from.
 *
 * @return array
 *  An array representation of the article's componentLayouts, ready to be json_encode'd.
 */
function publish_to_apple_news_generate_component_layouts($article_type) {
  $json = array();

  $layoutsQuery = new EntityFieldQuery();
  $layoutsResult = $layoutsQuery->entityCondition('entity_type', 'publish_to_apple_news_component_layout')
    ->propertyCondition('tid', $article_type->tid)
    ->execute();

  if (isset($layoutsResult['publish_to_apple_news_component_layout'])) {
    $lids = array_keys($layoutsResult['publish_to_apple_news_component_layout']);
    $layouts = entity_load('publish_to_apple_news_component_layout', $lids);

    foreach ($layouts as $layout) {
      $json_layout = array();

      if (!empty($layout->column_start)) {
        $json_layout['columnStart'] = (int)$layout->column_start;
      }
      if (!empty($layout->column_span)) {
        $json_layout['columnSpan'] = (int)$layout->column_span;
      }
      if (!empty($layout->margin_top) || !empty($layout->margin_bottom)) {
        $json_layout['margin'] = array();
        if (!empty($layout->margin_top)) {
          $json_layout['margin']['top'] = (int)$layout->margin_top;
        }
        if (!empty($layout->margin_bottom)) {
          $json_layout['margin']['bottom'] = (int)$layout->margin_bottom;
        }
      }
      $json_layout['contentInset'] = array(
        'top' => ($layout->content_inset_top) ? true : false,
        'right' => ($layout->content_inset_right) ? true : false,
        'bottom' => ($layout->content_inset_bottom) ? true : false,
        'left' => ($layout->content_inset_left) ? true : false,
      );
      if (!empty($layout->ignore_document_margin)) {
        $json_layout['ignoreDocumentMargin'] = $layout->ignore_document_margin;
      }
      if (!empty($layout->ignore_document_gutter)) {
        $json_layout['ignoreDocumentGutter'] = $layout->ignore_document_gutter;
      }
      if (!empty($layout->minimum_height)) {
        $json_layout['minimumHeight'] = $layout->minimum_height;
      }

      if (!empty($json_layout)) {
        $json[$layout->identifier] = $json_layout;
      }
    }
  }

  return $json;
}

/**
 * Generate the json for the article's componentStyles.
 *
 * @param $article_type
 *  The fully-loaded article type to get the styles from.
 * @param $node
 *  The fully-loaded node to replace tokens with.
 * @param $files
 *  An array of full file paths to append to.
 * @param $failedStyles
 *  If a style resolves to empty, add to this array so components won't reference them.
 *
 * @return array
 *  An array representation of the article's componentStyles, ready to be json_encode'd.
 */
function publish_to_apple_news_generate_component_styles($article_type, $node, &$files, &$failedStyles) {
  $json = array();

  $stylesQuery = new EntityFieldQuery();
  $stylesResult = $stylesQuery->entityCondition('entity_type', 'publish_to_apple_news_component_style')
    ->propertyCondition('tid', $article_type->tid)
    ->execute();

  if (isset($stylesResult['publish_to_apple_news_component_style'])) {
    $sids = array_keys($stylesResult['publish_to_apple_news_component_style']);
    $styles = entity_load('publish_to_apple_news_component_style', $sids);

    foreach ($styles as $style) {
      $jsonStyle = array();

      if (!empty($style->background_color)) {
        $jsonStyle['backgroundColor'] = $style->background_color;
      }

      if (!empty($style->opacity)) {
        $jsonStyle['opacity'] = (int)$style->opacity;
      }

      $fill = array();
      if (!empty($style->image_fill_url)) {
        $fill = publish_to_apple_news_generate_fill('image', $style, $node, $files);
      } else if (!empty($style->video_fill_url)) {
        $fill = publish_to_apple_news_generate_fill('video', $style, $node, $files);
      }
      if (!empty($fill)) {
        $jsonStyle['fill'] = $fill;
      }

      if (!empty($style->border_color)) {
        $jsonStyle['border'] = array(
          'all' => array(
            'color' => $style->border_color,
            'width' => $style->border_width,
            'style' => $style->border_style,
          ),
          'top' => ($style->border_top) ? TRUE : FALSE,
          'bottom' => ($style->border_bottom) ? TRUE : FALSE,
          'left' => ($style->border_left) ? TRUE : FALSE,
          'right' => ($style->border_right) ? TRUE : FALSE,
        );
      }

      if (!empty($jsonStyle)) {
        $json[$style->identifier] = $jsonStyle;
      } else {
        $failedStyles[] = $style->identifier;
      }
    }
  }

  return $json;
}

/**
 * Generate JSON for image and video fills on component styles
 *
 * @param $type - 'image' or 'video'
 * @param $style - The component style entity.
 * @param $node - Node to use for replacement tokens.
 * @param $files - files array to be appended to for images.
 */
function publish_to_apple_news_generate_fill($type, $style, $node, &$files) {
  $fill = array();

  // Get image data for image fill or video's still image
  $image_field_value = $type == 'image' ? $style->image_fill_url : $style->video_fill_still_url;
  if (!empty($image_field_value)) {
    $image_url = publish_to_apple_news_process_text_value($image_field_value, array('node' => $node));
    $image_data = publish_to_apple_news_convert_image($image_url);
  }

  // Process token in video URL
  if ($type == 'video') {
    $video_url = publish_to_apple_news_process_text_value($style->video_fill_url, array('node' => $node));
  }

  // Generate if an image or video is present
  if (($type == 'image' && $image_data) || ($type == 'video' && !empty($video_url))) {
    $fill = array(
      'type' => $type,
      'URL' => $type == 'image' ? $image_data['bundle'] : $video_url,
    );

    if (!empty($style->{$type . '_fill_attachment'})) {
      $fill['attachment'] = $style->{$type . '_fill_attachment'};
    }

    if (!empty($style->{$type . '_fill_mode'})) {
      $fill['fillMode'] = $style->{$type . '_fill_mode'};
    }

    if (!empty($style->{$type . '_fill_vertical_alignment'})) {
      $fill['verticalAlignment'] = $style->{$type . '_fill_vertical_alignment'};
    }

    if (!empty($style->{$type . '_fill_horizontal_alignment'})) {
      $fill['horizontalAlignment'] = $style->{$type . '_fill_horizontal_alignment'};
    }

    // Add video-specific fields
    if ($type == 'video') {
      if ($image_data) {
        $fill['stillURL'] = $image_data['bundle'];
      }
      if (!empty($style->video_fill_loop)) {
        $fill['loop'] = $style->video_fill_loop ? TRUE : FALSE;
      }
    }

    // Add image real path to files array
    if ($image_data) {
      if (!in_array($image_data['realpath'], $files)) {
        $files[] = $image_data['realpath'];
      }
    }
  }

  return $fill;
}

/**
 * Generate the json for the article's componentTextStyles.
 *
 * @param $article_type
 *  The fully-loaded article type to get the text styles from.
 *
 * @return array
 *  An array representation of the article's componentTextStyles, ready to be json_encode'd.
 */
function publish_to_apple_news_generate_component_text_styles($article_type) {
  $json = array();

  $textStylesQuery = new EntityFieldQuery();
  $textStylesResult = $textStylesQuery->entityCondition('entity_type', 'publish_to_apple_news_component_text_style')
    ->propertyCondition('tid', $article_type->tid)
    ->execute();

  if (isset($textStylesResult['publish_to_apple_news_component_text_style'])) {
    $tsids = array_keys($textStylesResult['publish_to_apple_news_component_text_style']);
    $text_styles = entity_load('publish_to_apple_news_component_text_style', $tsids);

    foreach ($text_styles as $text_style) {
      $json[$text_style->identifier] = array(
        'underline' => ($text_style->underline) ? TRUE : FALSE,
        'strikethrough' => ($text_style->strikethrough) ? TRUE : FALSE,
        'hyphenation' => ($text_style->hyphenation) ? TRUE : FALSE,
        'hangingPunctuation' => ($text_style->hanging_punctuation) ? TRUE : FALSE,
      );

      if (!empty($text_style->vertical_alignment)) {
        $json[$text_style->identifier]['verticalAlignment'] = $text_style->vertical_alignment;
      }

      if (!empty($text_style->text_alignment)) {
        $json[$text_style->identifier]['textAlignment'] = $text_style->text_alignment;
      }

      if (!empty($text_style->font_name)) {
        $json[$text_style->identifier]['fontName'] = $text_style->font_name;
      }

      if (!empty($text_style->font_size)) {
        $json[$text_style->identifier]['fontSize'] = (int)$text_style->font_size;
      }

      if (!empty($text_style->line_height)) {
        $json[$text_style->identifier]['lineHeight'] = (int)$text_style->line_height;
      }

      if (!empty($text_style->paragraph_spacing_before)) {
        $json[$text_style->identifier]['paragraphSpacingBefore'] = (int)$text_style->paragraph_spacing_before;
      }

      if (!empty($text_style->paragraph_spacing_after)) {
        $json[$text_style->identifier]['paragraphSpacingAfter'] = (int)$text_style->paragraph_spacing_after;
      }

      if (!empty($text_style->first_line_indent)) {
        $json[$text_style->identifier]['firstLineIndent'] = (int)$text_style->first_line_indent;
      }

      if (!empty($text_style->vertical_alignment)) {
        $json[$text_style->identifier]['verticalAlignment'] = $text_style->vertical_alignment;
      }

      if (!empty($text_style->text_alignment)) {
        $json[$text_style->identifier]['textAlignment'] = $text_style->text_alignment;
      }

      if (!empty($text_style->text_color)) {
        $json[$text_style->identifier]['textColor'] = $text_style->text_color;
      }

      if (!empty($text_style->text_transform)) {
        $json[$text_style->identifier]['textTransform'] = $text_style->text_transform;
      }

      if (!empty($text_style->background_color)) {
        $json[$text_style->identifier]['backgroundColor'] = $text_style->background_color;
      }

      if (!empty($text_style->tracking)) {
        $json[$text_style->identifier]['tracking'] = (float)$text_style->tracking;
      }

      if (!empty($text_style->link_style_font_name)) {
        $link_style = array(
          'underline' => ($text_style->link_style_underline) ? true : false,
        );

        if (!empty($text_style->link_style_font_name)) {
          $link_style['fontName'] = $text_style->link_style_font_name;
        }
        if (!empty($text_style->link_style_font_size)) {
          $link_style['fontSize'] = (int)$text_style->link_style_font_size;
        }

        if (!empty($text_style->link_style_text_color)) {
          $link_style['textColor'] = $text_style->link_style_text_color;
        }

        $json[$text_style->identifier]['linkStyle'] = $link_style;
      }

      if (!empty($text_style->dropcap_number_of_lines)) {
        $dropcap_style = array();

        if (!empty($text_style->dropcap_number_of_lines)) {
          $dropcap_style['numberOfLines'] = (int)$text_style->dropcap_number_of_lines;
        }
        if (!empty($text_style->dropcap_number_of_characters)) {
          $dropcap_style['numberOfCharacters'] = (int)$text_style->dropcap_number_of_characters;
        }
        if (!empty($text_style->dropcap_number_of_raised_lines)) {
          $dropcap_style['numberOfRaisedLines'] = (int)$text_style->dropcap_number_of_raised_lines;
        }
        if (!empty($text_style->dropcap_font_name)) {
          $dropcap_style['fontName'] = $text_style->dropcap_font_name;
        }
        if (!empty($text_style->dropcap_text_color)) {
          $dropcap_style['textColor'] = $text_style->dropcap_text_color;
        }
        if (!empty($text_style->dropcap_padding)) {
          $dropcap_style['padding'] = (int)$text_style->dropcap_padding;
        }
        if (!empty($text_style->dropcap_background_color)) {
          $dropcap_style['backgroundColor'] = $text_style->dropcap_background_color;
        }

        if (!empty($dropcap_style)) {
          $json[$text_style->identifier]['dropCapStyle'] = $dropcap_style;
        }
      }

      $text_shadow = array();
      if (!empty($text_style->text_shadow_color)) {
        $text_shadow['color'] = $text_style->text_shadow_color;
      }
      if (!empty($text_style->text_shadow_radius)) {
        $text_shadow['radius'] = (int)$text_style->text_shadow_radius;
      }
      if (!empty($text_style->text_shadow_opacity)) {
        $text_shadow['opacity'] = (float)$text_style->text_shadow_opacity;
      }
      if (!empty($text_style->text_shadow_offset_x)) {
        $text_shadow['offset'] = array(
          'x' => (int)$text_style->text_shadow_offset_x,
          'y' => (int)$text_style->text_shadow_offset_y,
        );
      }

      if (!empty($text_shadow)) {
        $json[$text_style->identifier]['textShadow'] = $text_shadow;
      }
    }
  }

  return $json;
}
