INTRODUCTION
------------
This module provides Drupal integration with Apple's News platform. It allows you to easily configure the layout of nodes published to News via the Apple News API. When a node is created or updated, it is converted to Apple's new JSON-based Native format with your layout configuration and posted to your channel.

 * For an overview of how to configure the module, check out our blog post at http://projectricochet.com/blog/publishing-drupal-content-apple-news-api

Learn more about News here: https://www.apple.com/news/

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/publish_to_apple_news


REQUIREMENTS
------------
This module requires the following modules:
 * Entity API (https://drupal.org/project/entity)
 * Token (https://drupal.org/project/token)


IOS 10 NOTES
------------
There are a few new features included in the module that are only supported in iOS 10. Previewing these will require the latest version of Xcode software. They include:
* Facebook and Vine Post components
* Cover Art (specified in the article type settings)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * First, you'll need to visit /admin/config/services/publish-to-apple-news.

   - Enter a live base URL, which should be the $base_url of either production site or a reachable development site.
   - Enter at minimum your API key and shared secret.
   - Enter a default channel ID.
   - Click "Refresh Section ID list" button.
   - Choose a Default section ID.
   - Save the form.

 * To get started quickly, create an Apple News article type from a template.

   - Visit /admin/structure/publish-to-apple-news/create-template.
   - Select the node type to create an article type for.
   - Select a left, center, or right aligned template and click "Create".
   - Add or edit a node of the previously selected node type.
   - Under "Apple News Settings" check "Publish to Apple News" and save the node.
   - That's it! The node will be converted into an Apple News article and pushed via the API.
   - NOTE: These default article types assume your nodes have a field_image that it can use for the thumbnail and header image in the article. If you don't have an image field, follow these steps:
     - Click "Settings" next to the article type at /admin/structure/publish-to-apple-news
     - Remove "[node:field_image]" in the "Thumbnail URL" text box and click "Save".
     - Click "Configure" next to the article type (/admin/structure/publish-to-apple-news) and delete the "Header" component.
     - Click the "Component Layouts" and delete "headerLayout".
     - Click "Component Styles" and delete "headerStyle".

 * Configuring and publishing to Apple News

   - Note: In-depth documentation of the Apple News Format can be found here - https://developer.apple.com/library/ios/documentation/General/Conceptual/Apple_News_Format_Ref/index.html

   1) Configure the Apple News permissions at /admin/people/permissions. The available permissions are:
      - Publish to Apple News
      - Manage Apple News article types
      - Configure Apple News settings
      - View Apple News posted articles

   2) Create a new article type at /admin/structure/publish-to-apple-news/create.
      - Fill out the required fields.
      - Some fields allow you to enter tokens, which will have their values replaced during the conversion process.
      - Note: Apple *highly* recommends leaving the layout configuration as-is in order to assure your article will be viewable across all iOS devices.
      - Click "Save".

   3) Configure your article type's layouts, styles, and text styles.
      - Under the "Configure" section for your article type you will see tabs for "Component Layouts", "Component Styles", and "Component Text Styles".
        - Component Layouts define the layout information for a component.
        - Component Styles define styling properties for a component.
        - Component Text Styles define text styling for a component.

   4) Configure your article type's components.
      - Visit /admin/structure/publish-to-apple-news and click "Configure".
      - Under "Add new component", select a component type and click "Add".
      - Fill out the fields for the component, optionally relating it to a component layout, component style, and component text style (if applicable).
      - Click "Save".
      - If the component you are editing accepts other components (header, section, and chapter), you'll see an "Add Components" button. Components are infinitely nestable.
      - You can re-order components within their parent by dragging and dropping the component fieldset's title. The new order is saved instantly via AJAX.

   5) Preview the article
      - Download and install Apple's News Preview tool - https://developer.apple.com/news-preview/
      - Included in this module is a handy preview function that will take a node and generate a bundled Apple News zip file.
      - Visit /admin/structure/publish-to-apple-news.
      - Click "Preview" next to the desired Apple News article type.
      - Select a node.
      - Click "Download".
      - Drag and drop the downloaded zip file into News Preview to see exactly how it will look in the Apple News app.

   6) Publish to Apple News!
      - Add or edit a node as you normally would in Drupal.
      - If there is an Apple News article type for the type of node you're adding, you'll see an "Apple News Settings" box.
      - Check "Publish to Apple News". If previously published, the checkbox will be checked, and will include the article status.
      - Check "Is preview?" if you would like to publish the article to preview directly on the Apple News app.
      - Uncheck "Is preview?" to make an article live that was previously in preview mode.
      - Select one or more Apple News sections to publish the article to. These sections are managed in Apple News itself.
      - Optionally add some accessory text and/or a maturity rating.
      - Save! The node will immediately be pushed via the Apple News API.

  7) View the posted articles report
      - Visit /admin/reports/publish-to-apple-news.
      - The "Processing" tab displays articles that have not yet been confirmed as live by Apple News.
      - Click "Check Now" to make an API call to Apple News and get the status of the articles in the table.
      - The "Completed" tab shows articles that are either live or have failed processing.

